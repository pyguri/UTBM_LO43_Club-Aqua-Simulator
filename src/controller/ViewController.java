package controller;

import model.*;
import processing.core.PApplet;
import view.*;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class ViewController extends PApplet {

    private static float FRAMERATE = 60;
    private static int WIDTH = 1000;
    private static int HEIGHT = 700;
    private static boolean BOIDS = false;
    private static float GAMESPEED = 1;

    private Drawer drawer;
    private Aquarium aquarium = new Aquarium(0.0,0.0,(double) WIDTH,(double) HEIGHT-132);

    private boolean pause;
    private int mode;

        /*C'est le mode d'activation: il correspond à quel objet on ajoute*/

    private static void getFramerate(){
        Scanner scan = new Scanner(System.in);
        try {
            System.out.print("Choose the framerate\n");
            FRAMERATE = scan.nextFloat();
        } catch (InputMismatchException e){
            System.out.print("not a float\n");
            getFramerate();
        }
    }

    public static void main(String[] args) {
        getFramerate();

        PApplet.main("controller.ViewController");
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public static float getFRAMERATE() {
        return FRAMERATE/GAMESPEED;
    }

    public static boolean getBOIDS() {
        return BOIDS;
    }

    public static float getGAMESPEED() {
        return GAMESPEED;
    }

    public void settings(){
        size(WIDTH,HEIGHT);
    }

    public void setup(){
        frameRate(FRAMERATE);
        drawer = new Drawer(this);
        pause = false;
        mode = 0;
    }

    public void draw() {
        if(!pause){                                                                                                     //game pause
            drawer.drawInterface();
            drawer.draw(aquarium);
            drawer.drawMode(mode);

            aquarium.live();
        } else {
            drawer.drawPause();
        }
    }

    public void keyReleased()
    {
        if (key == ' ')
            pause = !pause;
        if (key == 'd')
            drawer.debug();
        if (key == 'b' && !pause)
            BOIDS = !BOIDS;
        if (key == 'p' && !pause) {
            mode = 0;
            aquarium.purge();
        }
        if (keyCode == RIGHT && GAMESPEED < 5) GAMESPEED++;

        if (keyCode == LEFT && GAMESPEED > 1) GAMESPEED--;
    }

        public void mouseReleased()
    {
        int startInterfaceY = HEIGHT-100;
        if (!pause){
            if (mouseY >= startInterfaceY){     //Nourriture Saine
                mode = 1;
            }
            if (mouseY >= startInterfaceY && mouseX <= 125) { //Add Fish
                mode = 2;
            }
            if (mouseY >= startInterfaceY && mouseX >= 250 && mouseX <= 375) {  // Ciseau
                mode = 3;
            }
            if (mouseY >= startInterfaceY && mouseX >= 375 && mouseX <= 500) {  // Biere
                mode = 4;
            }
            if (mouseY >= startInterfaceY && mouseX >= 500 && mouseX <= 625) {  // Junk Food
                mode = 5;
            }
            if (mouseY >= startInterfaceY && mouseX >=625  && mouseX <= 750) {  //Cleaning Solution
                mode = 6;
            }
            if (mouseY >= startInterfaceY && mouseX >= 750) { //Manual
                mode = 7;
            }
            if (mouseY <= startInterfaceY && mode !=2) {
                addTool(mode);
            }
            if (mouseY <= startInterfaceY-32 && mode ==2) {
                Random rnd = new Random();
                float red = rnd.nextFloat()*255;
                float green = 200+rnd.nextFloat()*55;
                float blue =  255;
                double sizeX = 15 + rnd.nextFloat()*10;
                double sizeY = 10 + rnd.nextFloat()*10;
                aquarium.addFish(new Fish("generic fish", (double) mouseX, (double) mouseY,  sizeX,  sizeY, red,  green, blue));
            }
        }
    }

    private void addTool(int mode){
        switch (mode){
            case 1 : aquarium.addTool(new Sain(mouseX)); break;
            case 2 : break;
            case 3 : aquarium.addTool(new Scissors(mouseX,mouseY)); break;
            case 4 : aquarium.addTool(new Beer(mouseX)); break;
            case 5 : aquarium.addTool(new Junk(mouseX)); break;
            case 6 : aquarium.addTool(new CleaningSolution()); break;
            default: break;
        }

    }
}

