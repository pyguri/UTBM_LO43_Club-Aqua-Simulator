package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Vector;

public class Aquarium extends AbstractEntity {

   private int cleanliness;
    private int toxicity;

    private ArrayList<Fish> fishes;
    ArrayList<_Tool>  tools;

    public Aquarium()
    {
        super();
        cleanliness = 255;
        toxicity = 0;
        fishes = new ArrayList<>();
        tools = new ArrayList<>();
    }

    public Aquarium (Double positionX, Double positionY, Double sizeX, Double sizeY)
    {
        super(positionX, positionY, sizeX, sizeY);
        cleanliness = 255;
        toxicity = 0;
        fishes = new ArrayList<>();
        tools = new ArrayList<>();
    }

    public int getCleanliness() {
        return cleanliness;
    }

    public void setCleanliness(int cleanliness) {
        this.cleanliness = cleanliness;
    }

    public ArrayList<Fish> getFishes()
    {
        return fishes;
    }

    public ArrayList<_Tool> getTools()
    {
        return tools;
    }

    public void purge()
    {
        fishes.clear();
        tools.clear();
    }

    public void addFish(Fish f)
    {
        fishes.add(f);
        System.out.print("Fish added at : " + f.position + "\n");
    }

    public void addTool(_Tool t)
    {
        tools.add(t);
        System.out.print("Tool added at : " + t.position +"\n");
    }

    private boolean inAquarium(Vector<Double> pos)
    {
        return (pos.get(0) >= 0 && pos.get(0) <= size.get(0)) &&
                (pos.get(1) >= 0 && pos.get(1) <= size.get(1));
    }

    private void checkMove(AbstractEntity entity)
    {
        Vector<Double> nextMove = entity.nextMove();
        Vector<Double> newSpeed = new Vector<>(entity.getSpeed());

        if (!inAquarium(nextMove)) {
            if (nextMove.get(0) >= size.get(0)){
                newSpeed.set(0, -newSpeed.get(0));
            } else if (nextMove.get(0) <= 0){
                newSpeed.set(0, -newSpeed.get(0));
            }
            if (nextMove.get(1) >= size.get(1)){
                if (entity instanceof Food) newSpeed = zeroes(entity.getSpeed());
                else newSpeed.set(1, -newSpeed.get(1));
            } else if (nextMove.get(1) <= 0){
                newSpeed.set(1, -newSpeed.get(1));
            }
        }
       /* if(entity instanceof Fish){
            for (Fish fish : fishes) {
                if (((Fish) entity).inNextBox(fish, 100) && fish != entity) {
                    nextMove = diffVector(nextMove, entity.getSpeed());
                }
            }*/

        entity.setSpeed(newSpeed);
    }

    public void remove(AbstractEntity entity)
    {
        fishes.remove(entity);
        tools.remove(entity);
    }

    @Override
    protected void live(Aquarium aquarium) {
        ArrayList<AbstractEntity> allEntities = perceive();

        for (AbstractEntity entity: allEntities){                                                                       //get influences
            entity.live(this);
        }
        for (AbstractEntity entity: allEntities){                                                                       //resolve conflicts
            checkMove(entity);
        }
        for (AbstractEntity entity: allEntities){                                                                       //make moves
            entity.move();
        }
    }

    public void live()
    {
        live(null);
    }

    @Override
    public ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities) {
        ArrayList<AbstractEntity> _allEntities = new ArrayList<>(fishes);
        _allEntities.addAll(tools);
        _allEntities.sort(Comparator.comparingDouble(this::distanceFrom));
        return _allEntities;
    }

    public ArrayList<AbstractEntity> perceive() {return perceive(null);}


}