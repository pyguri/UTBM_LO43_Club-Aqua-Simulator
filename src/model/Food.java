package model;

import controller.ViewController;

import java.util.ArrayList;

abstract public class Food extends _Tool {
    double qty;
    double nutr;

    Food(double x)
    {
        super(x,0,0,0);
    }

    private void setSpeed() {
        speed.set(0, Math.sin(2*Math.PI*position.get(1)/5));
        speed.set(1, speed.get(1) + 0.3/ ViewController.getFRAMERATE()/* - speed.get(1)/ ViewController.getFRAMERATE()*/);
    }

    public double getQty()
    {
        return qty;
    }

    public double getNutr()
    {
        return nutr;
    }

    public void decrQty(double i)
    {
        qty -= i / ViewController.getFRAMERATE();
    }

    @Override
    void live(Aquarium aquarium)
    {
        if(qty <= 0){
            aquarium.remove(this);
        }else{
            decrQty(1/60);
            setSpeed();
        }
    }


    @Override
    ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities)
    {
        return null;
    }
}