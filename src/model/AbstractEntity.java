package model;

import java.util.ArrayList;
import java.util.Vector;

abstract public class AbstractEntity {

    Vector<Double> size;
    Vector<Double> position;
    Vector<Double> speed;

    AbstractEntity() {
        size = new Vector<>(2);
        position = new Vector<>(2);
        speed = new Vector<>(2);
    }

    AbstractEntity( double posX, double posY, double sizeX, double sizeY) {
        position = new Vector<>(2);
        position.add(posX);
        position.add(posY);

        size = new Vector<>(2);
        size.add(sizeX);
        size.add(sizeY);

        speed = new Vector<>(zeroes(position));
    }

    public Vector<Double> getSize() {
        return size;
    }

    public Vector<Double> getPosition() {
        return position;
    }

    private void setPosition(Vector<Double> position)
    {
        this.position = position;
    }

    public Vector<Double> getSpeed()
    {
        return speed;
    }

    public void setSpeed(Vector<Double> speed) {
        this.speed = speed;
    }

    public void move() {
        setPosition(nextMove());
    }

    public Vector<Double> nextMove()
    {
        return sumVector(position, speed);
    }

    abstract void live(Aquarium aquarium);

    abstract ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities);







    double distanceFrom(AbstractEntity entity)
    {
        return distanceFrom(entity.position);
    }

    double distanceFrom(Vector<Double> pos)
    {
        Vector<Double> relPos = diffVector(pos, position);
        double sum = 0.0;
        for (int i = 0; i < 2; ++i) {
            sum += (relPos.get(i)) * (relPos.get(i));
        }

        return Math.sqrt(sum);
    }

    static Vector<Double> zeroes(Vector<Double> v)
    {
        return scalProduct(v, 0.0);
    }

    static Vector<Double> sumVector(Vector<Double> v1, Vector<Double> v2)
    {
        Vector<Double> sum = new Vector<>(v1);
        for (int i = 0; i < sum.size(); i++) {
            sum.set(i, sum.get(i) + v2.get(i));
        }
        return sum;
    }

    static Vector<Double> diffVector(Vector<Double> v1, Vector<Double> v2)                                              //v1 - v2
    {
        Vector<Double> diff = new Vector<>(v1);
        for (int i = 0; i < diff.size(); i++) {
            diff.set(i, diff.get(i) - v2.get(i));
        }
        return diff;
    }

    static Vector<Double> scalProduct(Vector<Double> v, double scalar)
    {
        Vector<Double> prod = new Vector<>(v);
        for (int i = 0; i < prod.size(); i++) {
            prod.set(i, prod.get(i) * scalar);
        }
        return prod;
    }

    static boolean containsInstanceOf(ArrayList<AbstractEntity> arrayList, Class<? extends AbstractEntity> aClass)
    {
        return arrayList.stream().anyMatch(aClass::isInstance);
    }
}