package model;

import java.util.ArrayList;

public abstract class _Tool extends AbstractEntity
{
    _Tool()
    {
        super();
    }
    _Tool(double _posX, double _posY, double _sizeX, double _sizeY )
    {
        super(_posX,_posY,_sizeX,_sizeY);
    }

    @Override
    abstract void live(Aquarium a);

    @Override
    abstract ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities);
}