package model;

import java.util.ArrayList;
import java.util.Comparator;

public class Scissors extends _Tool {

  public Scissors(){
    super();
  }

  public Scissors(double posX, double posY)
  {
    super(posX,posY,20,20);
  }

  @Override
  public void live(Aquarium aquarium) {
    ArrayList<AbstractEntity> perceived = perceive(aquarium.perceive());
    if(!perceived.isEmpty())
    {
        Fish f = (Fish) perceived.get(0);
        if(f.inHitBox(this, 200)){

            f.setHP(f.getHP()-5);
        }
    }
      aquarium.tools.remove(this);
  }

  @Override
  ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities) {
    ArrayList<AbstractEntity> perceived = new ArrayList<>();

    for (AbstractEntity e : allEntities) {
          if (e instanceof  Fish)
          {
              perceived.add(e);
          }
      }

    perceived.sort(Comparator.comparingDouble(this::distanceFrom));

    return perceived;
  }


}