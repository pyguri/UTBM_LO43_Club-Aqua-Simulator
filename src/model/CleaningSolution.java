package model;

import java.util.ArrayList;

public class CleaningSolution extends _Tool {

    public CleaningSolution()
    {
        super();
    }
  @Override
  public void live(Aquarium aquarium){
    aquarium.setCleanliness(255);
  }

  @Override
  public ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities) {
    return null;
  }
}