package model;

import controller.ViewController;

import java.util.*;
import static java.lang.Math.abs;


public class Fish extends AbstractEntity {
    private String name;
    private double HP;
    private double maxHP;
    private double fullness;
    private double maxFullness;
    private double nominalSpeed;                                                                                        //in pixel/sec
    private int age;
    private int anger;
    private int strength;
    private double perceptionRange;                                                                                     //in pixels
    private double perceptionAngle;                                                                                     //in degree
    private Vector<Float> color;

    public Fish(){
        super();
        color = new Vector<>(3);
    }

    public Fish(String _name, Double posX, Double posY, Double sizeX, Double sizeY, Float red, Float green, Float blue)
    {
        super(posX, posY, sizeX, sizeY);
        name = _name;
        HP = 20;
        maxHP = 30;
        fullness = 50;
        maxFullness = 100;
        nominalSpeed = 200;
        perceptionRange = 300;
        perceptionAngle = 90;

        color = new Vector<>(3);
        color.add(red);
        color.add(green);
        color.add(blue);
    }

/*
    private Fish(Fish _fish) {
        super(_fish.position.get(0), _fish.position.get(1),
                _fish.size.get(0), _fish.size.get(1));
        speed = _fish.speed;
    }
*/

    private void setSpeed(Vector<Double> pos, double ratio)
    {
        Vector<Double> newSpeed = new Vector<>(speed);

        double relPosI;

        for (int i = 0; i < speed.size(); i++) {
            relPosI = pos.get(i) - position.get(i);
            if (relPosI != 0) {
                newSpeed.set(i, (relPosI / abs(relPosI)) * (ratio / 100.0) * (nominalSpeed / ViewController.getFRAMERATE()));
            }else{
                newSpeed.set(i, 0.0);
            }
        }

        setSpeed(newSpeed);
    }

    public double getHP()
    {
        return HP;
    }

    private void decrHP(double i)
    {
        HP -= i/ViewController.getFRAMERATE();
    }

    private void incrHP(double i) {
        HP += i/ViewController.getFRAMERATE();
    }

    public void setHP(double HP)
    {
        this.HP = HP;
    }

    public double getMaxHP()
    {
        return maxHP;
    }

    public double getFullness()
    {
        return fullness;
    }

    private void decrFullness(double i)
    {
        fullness -= i/ViewController.getFRAMERATE();
    }

    private void incrFullness(double i)
    {
        fullness += i/ViewController.getFRAMERATE();
    }

    public double getMaxFullness()
    {
        return maxFullness;
    }

    public Vector<Float> getColor()
    {
        return color;
    }

    private double getAbsDirection()                                                                                    //renvoie l'angle du regard du poisson entre 0 et PI/2
    {
        return (Math.atan(abs(speed.get(1)/speed.get(0))));
    }

    public int getOrientation()                                                                                         //renvoie 1 si regarde a droite et -1 si regarde a gauche
    {
        int out = 1;
        Random r = new Random();
        if (r.nextBoolean()){out = -1;}else{out = 1;}

        if (speed.get(0) < 0) {
            out = -1;
        }
        if (speed.get(0) > 0){
            out = 1;
        }

        return out;
    }

    private boolean eat(Food food)
    {
        boolean out;
        if (food.getQty() > 0){
            incrFullness(food.getNutr());
            food.decrQty(1);
            out = true;
        }else{
            out = false;
        }
        return out;
    }

    private boolean inSight(AbstractEntity target, double angle, double range)                                          //angle in degree, range in pixel
    {
        double relPosX = target.position.get(0) - position.get(0);
        double relPosY = target.position.get(1) - position.get(1);
        double relAngle = Math.atan(Math.abs(relPosY) / relPosX) - getAbsDirection();
        boolean inRange, inAngle;

        double sum =(relPosX*relPosX) + (relPosY*relPosY);
        inRange = sum <= range*range;
        inAngle =  abs(relAngle) <= Math.toRadians(angle / 2) && relPosX / Math.abs(relPosX) == getOrientation();

        if(angle > 180.0){
            inAngle = !(abs(relAngle) <= Math.toRadians(180 - (angle/ 2)) && relPosX / Math.abs(relPosX) != getOrientation());
        }

        return  inRange && inAngle && target != this;
    }

    public boolean inHitBox(AbstractEntity entity, double size)                                                         //size in percent of the fish shape (ellipse)
    {
        double relPosX = entity.position.get(0) - position.get(0);
        double relPosY = entity.position.get(1) - position.get(1);
        double sizeX = this.size.get(0);
        double sizeY = this.size.get(1);
        boolean inRange;

        inRange = (relPosX*relPosX)/((sizeX/2)*(sizeX/2)) + (relPosY*relPosY)/((sizeY/2)*(sizeY/2)) <= (size/100)*(size/100);

        return inRange && entity != this;
    }

/*
    public boolean inNextBox(AbstractEntity entity, double size){
        Fish nextSelf = new Fish(this);
        nextSelf.move();

        return nextSelf.inHitBox(entity, size);
    }
*/

    private void foids(ArrayList<AbstractEntity> perceived)                                                             //F-ish dr-OIDS (pretty much the same as B-ird dr-OIDS)
    {
        if( containsInstanceOf(perceived,Fish.class)) {
            ArrayList<Fish> fishPerceived = new ArrayList<>();
            for (AbstractEntity entity : perceived) {
                if (entity instanceof Fish) {
                    fishPerceived.add((Fish) entity);
                }
            }

            Vector<Double> sumRule = cohesion(fishPerceived);
            sumRule = sumVector(sumRule, repulsion(fishPerceived));
            //sumRule = sumVector(sumRule, rule3(fishPerceived));

            Vector<Double> inertia = scalProduct(speed, ViewController.getFRAMERATE()/10);

            setSpeed(sumVector(inertia, scalProduct(sumRule, 1/ViewController.getFRAMERATE())));
        }
    }

    private Vector<Double> cohesion(ArrayList<Fish> perceived){                                                         //rule that drags the fish to the barycenter of the group perceived
        Vector<Double> gCenter = new Vector<>(zeroes(position));
        Vector<Double> relPos;
        int k = 2;                                                                                                      //urge to rally the gCenter

        for (AbstractEntity entity: perceived) {
            if(entity != this){
                relPos = diffVector(entity.position, position);
                gCenter = sumVector(gCenter, relPos);
            }
        }
        gCenter = scalProduct(gCenter, 1/perceived.size());
        gCenter = scalProduct(gCenter, k/distanceFrom(gCenter));
        return gCenter;
    }

    private Vector<Double> repulsion(ArrayList<Fish> perceived){                                                        //rule that make the fish dodge the others
        Vector<Double> dodge = new Vector<>(zeroes(position));
        Vector<Double> relPos;
        int k = 100;                                                                                                     //urge to dodge the other fishes

        for (AbstractEntity entity: perceived) {
            if (entity != this){
                    relPos = diffVector(entity.position, position);
                    dodge = sumVector(dodge, scalProduct(relPos, k/distanceFrom(entity)));
            }
        }
        return dodge;
    }

/*
    private Vector<Double> rule3(ArrayList<Fish> perceived){
        Vector<Double> avgSpeed = new Vector<>(zeroes(position));

        for (AbstractEntity entity: perceived) {
            if (entity instanceof Fish && entity != this) {
               avgSpeed = sumVector(avgSpeed, entity.speed);
            }
        }
        avgSpeed = scalProduct(avgSpeed, 1/perceived.size());
        System.out.println("3" + avgSpeed);
        return avgSpeed;
    }
*/

    private void food(ArrayList<AbstractEntity> perceived)
    {
        if (containsInstanceOf(perceived, Food.class) && fullness < 0.8*maxFullness) {
            AbstractEntity target = perceived.get(0);
            Iterator<AbstractEntity> i = perceived.iterator();
            while (!(target instanceof Food) && i.hasNext()) {
                target = i.next();
            }
            if (inHitBox(target, 110) && target instanceof Food) {
                boolean eating;
                do {
                    eating = eat((Food) target);
                } while (fullness < maxFullness && eating);
            } else {
                setSpeed(target.position, 100);
            }
        }
            if (fullness >= 0.8*maxFullness && HP < maxHP) {
                incrHP(1);
            }
            if (fullness <= 0) {
                decrHP(1);
            } else {
                decrFullness(1);
            }
        }

    private void fight(ArrayList<AbstractEntity> perceived) {
    }

    private void reproduce(ArrayList<AbstractEntity> perceived) {

    }

    void live(Aquarium aquarium)
    {
        if(HP <= 0) {
            aquarium.remove(this);
        }else{
            Vector<Double> front = new Vector<>(position);
            front.set(0, position.get(0) + (double) getOrientation());
            setSpeed(front, 50.0);

            ArrayList<AbstractEntity> perceived = perceive(aquarium.perceive());

            if (ViewController.getBOIDS()){foids(perceived);}
            food(perceived);
            fight(perceived);
            reproduce(perceived);
        }
    }

    ArrayList<AbstractEntity> perceive(ArrayList<AbstractEntity> allEntities)                                           //Returns a vector of entities in order of distance (PriorityQueue<T>)
    {
        ArrayList<AbstractEntity> perceived = new ArrayList<>();

        for (AbstractEntity entity : allEntities) {
            if (inSight(entity, perceptionAngle, perceptionRange) || inSight(entity, 360, 100)) {
                perceived.add(entity);
            }
        }
        perceived.sort(Comparator.comparingDouble(this::distanceFrom));
        return perceived;
    }
}