package model;

public class Beer extends Food {

  public Beer(double x) {
    super(x);
    qty = 10.0;
    nutr = -5.0;
  }
}