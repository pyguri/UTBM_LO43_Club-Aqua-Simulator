package view;

import controller.ViewController;
import model.*;

import processing.core.PApplet;

public class Drawer {
    private PApplet pApplet;
    private boolean debug = false;

    public Drawer(PApplet _pApplet){
        pApplet = _pApplet;
    }

    public void debug(){debug = !debug; }

    public void drawInterface(){
        pApplet.background(200);
        pApplet.stroke(0, 127, 255);
        pApplet.textSize(14);
        pApplet.fill(0);
        pApplet.text("Fish", 50, 650);
        pApplet.text("Food", 175, 650);
        pApplet.text("Scissors", 300, 650);
        pApplet.text("Beer", 425, 650);
        pApplet.text("Junk Food", 550, 650);
        pApplet.text("Clean", 675, 650);
        pApplet.text("Manual", 800, 650);
    }

    public void drawMode(int mode){
        pApplet.fill(0);
        if (mode == 2){
            pApplet.text("Add a fish", 0, 0, 500, 500);
        }
        if(mode == 1){
            pApplet.text("Add food", 0, 0, 500, 500);
        }
        if(mode == 3){
            pApplet.text("Scissors", 0, 0, 500, 500);
        }
        if(mode == 4){
            pApplet.text("Add Beer", 0, 0, 500, 500);
        }
        if(mode == 5){
            pApplet.text("Add Junk Food", 0, 0, 500, 500);
        }
        if(mode == 6){
            pApplet.text("Add Cleaning Solution", 0, 0, 500, 500);
        }
        if(mode == 7){
            pApplet.text("Manual", 0, 0, 500, 500);
        }


    }

    public void drawPause()
    {
        pApplet.fill(255,0,0);
        pApplet.textSize(120);
        pApplet.textAlign(0,0);
        pApplet.text("PAUSE", 0,0, ViewController.getWIDTH(), ViewController.getHEIGHT());
    }

    public void draw(Aquarium aquarium)
    {
        float x = (float) (double) aquarium.getPosition().get(0);
        float y = (float) (double) aquarium.getPosition().get(1);
        float sizeX = (float) (double) aquarium.getSize().get(0);
        float sizeY = (float) (double) aquarium.getSize().get(1);
        pApplet.fill(0, 127, 255);
        pApplet.rect(x, y, sizeX, sizeY);
        pApplet.fill(0,120,0,255-aquarium.getCleanliness());
        pApplet.rect(x, y, sizeX, sizeY);
        pApplet.stroke(255, 255, 127);
        pApplet.fill(255, 255, 127);
        pApplet.rect(sizeX, sizeY, -sizeX, 32);
        pApplet.fill(0);
        pApplet.text("Speed : " + (int) ViewController.getGAMESPEED(), 0, 600);
        for(Fish f : aquarium.getFishes()){
            this.draw(f);
        }
        for (_Tool t : aquarium.getTools()){
            if (t instanceof Food){
                this.draw((Food) t);
            }
        }
         if (debug){
            pApplet.fill(0);
            pApplet.text("Fishes : " + aquarium.getFishes().size(), 0, sizeY);
         }
    }

    private void draw(Fish f){
        float posX = (float) (double) f.getPosition().get(0);
        float posY = (float) (double) f.getPosition().get(1);
        float speedX = (float) (double) f.getSpeed().get(0);
        float speedY = (float) (double) f.getSpeed().get(1);
        float sizeX = (float) (double) f.getSize().get(0);
        float sizeY = (float) (double) f.getSize().get(1);
        float orientation = f.getOrientation();

        pApplet.colorMode(3);

        pApplet.ellipseMode(3);
        pApplet.fill(f.getColor().get(0),f.getColor().get(1),f.getColor().get(2));
        pApplet.noStroke();
        pApplet.ellipse(posX,posY,sizeX,sizeY);
        pApplet.triangle(posX-((sizeX/2)*orientation),posY,posX-(sizeX*orientation),posY+(sizeY),posX-(sizeX*orientation),posY-(sizeY));
        pApplet.colorMode(1);
        pApplet.fill(0);
        pApplet.ellipse(posX+((sizeX/5)*orientation),posY-(sizeY/5), 3, 3);

        if(debug) {
            pApplet.stroke(0);
            pApplet.fill(0);
            pApplet.rect(posX - sizeX / 2, posY - sizeY / 2 - 15, sizeX, 5);
            pApplet.noStroke();
            pApplet.fill(0, 255, 0);
            pApplet.rect(posX - sizeX / 2, posY - sizeY / 2 - 15, (float) (sizeX * (f.getHP() / f.getMaxHP())), 5);

            pApplet.stroke(0);
            pApplet.fill(0);
            pApplet.rect(posX - sizeX / 2, posY - sizeY / 2 - 10, sizeX, 5);
            pApplet.noStroke();
            pApplet.fill(255, 120, 120);
            pApplet.rect(posX - sizeX / 2, posY - sizeY / 2 - 10, (float) (sizeX * (f.getFullness() / f.getMaxFullness())), 5);

            pApplet.stroke(255, 0, 0);
            pApplet.line(posX, posY, posX+speedX*ViewController.getFRAMERATE(), posY+speedY*ViewController.getFRAMERATE());
        }

    }

    private void draw(Food f) {
        float posX = (float) (double) f.getPosition().get(0);
        float posY = (float) (double) f.getPosition().get(1);
        if (f instanceof Sain){
            draw((Sain) f);
        }
        if (f instanceof Junk){
            draw((Junk) f);
        }
        if (f instanceof Beer){
            draw((Beer) f);
        }
        if(debug) {
            pApplet.fill(0);
            pApplet.text((int) f.getQty(), posX-7, posY-10);
        }
    }

    private void draw(Sain s){
        float posX = (float) (double) s.getPosition().get(0);
        float posY = (float) (double) s.getPosition().get(1);
        pApplet.fill(200, 150, 150);
        pApplet.stroke(100,75,75);
        pApplet.ellipse(posX, posY,(float) s.getQty(),(float) s.getQty());

    }

    public void draw(Beer b){
        pApplet.fill(255,127,127);
        pApplet.stroke(255,0,0);
        pApplet.rect((float) (double) b.getPosition().get(0), (float) (double) b.getPosition().get(1),10,10);
    }

    public void draw(Junk j){
        float posX = (float) (double) j.getPosition().get(0);
        float posY = (float) (double) j.getPosition().get(1);
        pApplet.fill(150, 200, 150);
        pApplet.stroke(100,75,75);
        pApplet.ellipse(posX, posY,(float) j.getQty(),(float) j.getQty());
    }


}
